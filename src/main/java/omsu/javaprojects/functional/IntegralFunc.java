package omsu.javaprojects.functional;

import omsu.javaprojects.arity.Function1Arity;

public class IntegralFunc implements PolyarityFunctional {
    private double leftBound;
    private double rightBound;

    public IntegralFunc(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    public double functional(Function1Arity func) {
        if (func.getLeft() > leftBound  || rightBound > func.getRight()) {
            throw new IndexOutOfBoundsException();
        }
        double h = (rightBound - leftBound) / Config.n;
        double ans = 0;
        for (double i = 0; i < Config.n; i ++) {
            ans += func.getValueAtPoint(leftBound + h*i)*h;
        }
        return ans;
    }
}
